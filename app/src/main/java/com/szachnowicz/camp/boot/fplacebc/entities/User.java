package com.szachnowicz.camp.boot.fplacebc.entities;

/**
 * Created by Sebastian on 2017-08-18.
 */

 public class User {

    private String name;
    private String email;
    private String userID;

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public String getUserID() {

        return userID;
    }
}
