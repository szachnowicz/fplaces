package com.szachnowicz.camp.boot.fplacebc.entities;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Sebastian on 2017-08-22.
 */

public class SinglePoint {

    private double lat;
    private double lng;
    private String addres;

    public SinglePoint() {

    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAddres() {
        return addres;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }

    public LatLng getLatLng() {
        return new LatLng(lat, lng);
    }

    public void setPossiton(LatLng possiton) {
        setLat(possiton.latitude);
        setLng(possiton.longitude);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("SinglePoint{");
        sb.append("lat=").append(lat);
        sb.append(", lng=").append(lng);
        sb.append(", addres='").append(addres).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
