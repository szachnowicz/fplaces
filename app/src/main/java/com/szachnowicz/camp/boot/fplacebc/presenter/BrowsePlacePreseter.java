package com.szachnowicz.camp.boot.fplacebc.presenter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.ArrayAdapter;

import com.szachnowicz.camp.boot.fplacebc.R;
import com.szachnowicz.camp.boot.fplacebc.entities.Place;
import com.szachnowicz.camp.boot.fplacebc.presenter.impl.DetalisPresenter;
import com.szachnowicz.camp.boot.fplacebc.view.fragmentsCallback.BrowseMenuCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sebastian on 2017-08-23.
 */

public class BrowsePlacePreseter extends DetalisPresenter {

    private final BrowseMenuCallback menuCallback;

    public BrowsePlacePreseter(int ID, BrowseMenuCallback menuCallback) {
        super(ID);
        this.menuCallback = menuCallback;
    }

    public void bulidMapBulider(Context context, Place item) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        this.setPlace(item);
        builderSingle.setTitle(R.string.browsePlacesBuliderTitle);
        if (item.isSinglePointOnly()) {
            builderSingle.setIcon(R.drawable.big_red_pin);
        }
        else {
            builderSingle.setIcon(R.drawable.route_ico);
        }
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line);
        if (item.isSinglePointOnly()) {
            arrayAdapter.addAll(prepRouteDisplay(context));

        }
        else {
            arrayAdapter.addAll(prepSinglePoint(context));

        }

        builderSingle.setNegativeButton("wróc", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                menuCallback.onMenuActionClick(arrayAdapter.getItem(which));

            }
        });
        builderSingle.show();

    }

    private List<String> prepSinglePoint(Context context) {
        List<String> list = new ArrayList<>();
        list.add(context.getString(R.string.browsShowOnMap));
        list.add(context.getString(R.string.detalis));
        list.add(context.getString(R.string.browsEditName));
        list.add(context.getString(R.string.broweDelete));

        return list;
    }

    private List<String> prepRouteDisplay(Context context) {
        List<String> list = new ArrayList<>();
        list.add(context.getString(R.string.browsShowOnMap));
        list.add(context.getString(R.string.detalis));
        list.add(context.getString(R.string.browsEditName));
        list.add(context.getString(R.string.broweDelete));
        return list;
    }

    public void bulidDetalisBudlier(Context context) {
        Place item = this.getPlace();
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        this.setPlace(item);
        builderSingle.setTitle(R.string.detalis);
        if (item.isSinglePointOnly()) {
            builderSingle.setIcon(R.drawable.big_red_pin);
        }
        else {
            builderSingle.setIcon(R.drawable.route_ico);
        }


        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line);

        List<String> list = new ArrayList<>();
        list.add("Autor : " + item.getUser().getEmail());
        list.add("Data : " + item.getCreated());
        arrayAdapter.addAll(list);

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                

            }
        });

        builderSingle.setNegativeButton("wróc", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


        builderSingle.show();
    }

}
