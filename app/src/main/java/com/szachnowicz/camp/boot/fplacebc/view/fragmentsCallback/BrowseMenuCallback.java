package com.szachnowicz.camp.boot.fplacebc.view.fragmentsCallback;

/**
 * Created by Sebastian on 2017-08-23.
 */

public interface BrowseMenuCallback {
    void onMenuActionClick(String menuOption);

}
