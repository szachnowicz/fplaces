package com.szachnowicz.camp.boot.fplacebc.services.interfaces;

import com.google.firebase.database.DatabaseError;
import com.szachnowicz.camp.boot.fplacebc.entities.Place;

import java.util.List;

/**
 * Created by Sebastian on 2017-08-22.
 */

public interface DatabaseCallback {
    void displayResult(List<Place> list);

    void displayError(DatabaseError databaseError);
}
