package com.szachnowicz.camp.boot.fplacebc.presenter;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.szachnowicz.camp.boot.fplacebc.R;
import com.szachnowicz.camp.boot.fplacebc.entities.SinglePoint;
import com.szachnowicz.camp.boot.fplacebc.presenter.impl.DetalisPresenter;
import com.szachnowicz.camp.boot.fplacebc.services.MapCalculator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Sebastian on 2017-08-18.
 */

public class NewPlacePreseter extends DetalisPresenter {

    public final static int NP_PRES_ID = 1;

    public NewPlacePreseter() {
        super(NP_PRES_ID);

    }

    public static void disbaleEditText(EditText editText) {
        editText.setFocusable(false);
        editText.setEnabled(false);
        editText.setCursorVisible(false);
        editText.setKeyListener(null);
        editText.setBackgroundColor(Color.TRANSPARENT);
    }

    public void colectDataFormView(EditText ed1, EditText ed2) {
        place.setPlaceName(ed1.getText().toString());
        place.setPlaceDisc(ed2.getText().toString());
        place.setCreated(new Date());

    }


    public List<String> prepRouteDisplay() {
        List<String> listViewContent = new ArrayList<>();

        if (!place.getRouteList().isEmpty()) {
            listViewContent.clear();
            listViewContent.add("Trasa ma dłogość " + MapCalculator.getRouteLenghtInKm(place.getRouteList()) + " km");
            listViewContent.add("Składa sie z " + place.getRouteList().size() + " punktów " + System.lineSeparator() + " kilnij aby z focusować na mapie");

            for (SinglePoint latlng : place.getRouteList()) {
                listViewContent.add(latlng.toString());
            }

        }

        return listViewContent;
    }

    public void bulidListViewInToast(Context context) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
       // builderSingle.setIcon(R.drawable.big_red_pin);

        builderSingle.setTitle(context.getResources().getString(R.string.routeDetalis));
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line);
        if (!place.isSinglePointOnly()) {
            arrayAdapter.addAll(prepRouteDisplay());
            builderSingle.setIcon(R.drawable.route_ico);

        }
        else {
            arrayAdapter.addAll(prepSinglePoint());
            builderSingle.setIcon(R.drawable.big_red_pin);
            arrayAdapter.add(context.getResources().getString(R.string.takePictureItThisPlace));
        }

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builderSingle.show();
    }

    private List<String> prepSinglePoint() {
        List<String> listViewContent = new ArrayList<>();
        if (place.getSinglePoint() != null) {
            listViewContent.add(place.getSinglePoint().toString());


        }

        return listViewContent;
    }

    public void hideKeyboard(View view, Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    // return true if single point is null


    public void bulidBuliderForSearchAddresApi(Context context) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        builderSingle.setIcon(R.drawable.road_sing);
        builderSingle.setTitle(context.getResources().getString(R.string.addresOrStreet));

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.show();

    }

    public boolean routeOrSinglePoint() {
        return !place.getRouteList().isEmpty() && place.getSinglePoint() == null;
    }

}
