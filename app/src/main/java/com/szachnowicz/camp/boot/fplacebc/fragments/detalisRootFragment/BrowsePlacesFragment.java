package com.szachnowicz.camp.boot.fplacebc.fragments.detalisRootFragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.szachnowicz.camp.boot.fplacebc.R;
import com.szachnowicz.camp.boot.fplacebc.adapter.PlaceAdapter;
import com.szachnowicz.camp.boot.fplacebc.entities.Place;
import com.szachnowicz.camp.boot.fplacebc.presenter.BrowsePlacePreseter;
import com.szachnowicz.camp.boot.fplacebc.services.PlaceRepository;
import com.szachnowicz.camp.boot.fplacebc.services.interfaces.DatabaseCallback;
import com.szachnowicz.camp.boot.fplacebc.view.fragmentsCallback.BrowseMenuCallback;
import com.szachnowicz.camp.boot.fplacebc.view.fragmentsCallback.DetalisFragCallBack;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class BrowsePlacesFragment extends Fragment implements DatabaseCallback, BrowseMenuCallback {

    public static final int BIG_TEXT = 19;
    public static final int SMALL_TEXT = 11;
    public static final int FRAGMNET_ID = 3;

    @BindView(R.id.browseOtrherPlacesBtn)
    Button browseOtrherPlacesBtn;
    @BindView(R.id.browseUserPlacesBtn)
    Button browseUserPlacesBtn;
    @BindView(R.id.browPlacesListView)
    ListView browPlacesListView;
    @BindView(R.id.progressBarBrowsePlaces)
    ProgressBar progressBar;

    private PlaceRepository placeRepository;
    private boolean isUserPlaceDisplayed = true;
    private DetalisFragCallBack detalisFragCallBack;
    private List<Place> resultList = new ArrayList<>();
    private PlaceAdapter placeAdapter;
    private BrowsePlacePreseter browsePlacePreseter;
    private List<Place> currentUserPlaces;
    private List<Place> otherUserPlaces;

    public BrowsePlacesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        addListnerToButtons();
        placeRepository = new PlaceRepository();
        placeRepository.getAllResults(this);
        browsePlacePreseter = new BrowsePlacePreseter(FRAGMNET_ID, this);

        initAdapterAndAdptersListener();
    }

    private void initAdapterAndAdptersListener() {
        placeAdapter = new PlaceAdapter(getContext(), resultList);
        browPlacesListView.setAdapter(placeAdapter);

        browPlacesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                browsePlacePreseter.bulidMapBulider(getContext(), placeAdapter.getItem(position));
            }
        });

    }

    private void addListnerToButtons() {

        browseOtrherPlacesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isUserPlaceDisplayed) {
                    browseOtrherPlacesBtn.setTextColor(getContext().getResources().getColor(R.color.blue));
                    setListViewConntent(otherUserPlaces);

                    browseOtrherPlacesBtn.setTextSize(BIG_TEXT);
                    browseUserPlacesBtn.setTextColor(getContext().getResources().getColor(R.color.red));
                    browseUserPlacesBtn.setTextSize(SMALL_TEXT);
                    isUserPlaceDisplayed = !isUserPlaceDisplayed;

                }

            }
        });

        browseUserPlacesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isUserPlaceDisplayed) {
                    browseUserPlacesBtn.setTextColor(getContext().getResources().getColor(R.color.blue));
                    browseUserPlacesBtn.setTextSize(BIG_TEXT);
                    browseOtrherPlacesBtn.setTextColor(getContext().getResources().getColor(R.color.red));
                    browseOtrherPlacesBtn.setTextSize(SMALL_TEXT);
                    setListViewConntent(currentUserPlaces);
                    isUserPlaceDisplayed = !isUserPlaceDisplayed;

                }

            }
        });

    }

    private void setListViewConntent(List<Place> list) {
        if (list != null) {
            placeAdapter.clear();
            placeAdapter.addAll(list);
            placeAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_browse_places, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity;
        if (context instanceof Activity) {
            activity = (Activity) context;
            try {

                detalisFragCallBack = (DetalisFragCallBack) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                                                     + " must implement OnSelected");

            }
        }
    }

    @OnClick(R.id.goBackBtn1)
    public void onClickChangeViewButton(View view) {
        detalisFragCallBack.goBackToMenuFragment("");

    }

    public void hideUnesseryUI(boolean mapVisible) {

        if (mapVisible) {
            browseOtrherPlacesBtn.setVisibility(View.GONE);
            browseUserPlacesBtn.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            browPlacesListView.setVisibility(View.GONE);
        }
        else {
            browseOtrherPlacesBtn.setVisibility(View.VISIBLE);
            browseUserPlacesBtn.setVisibility(View.VISIBLE);
            browPlacesListView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void displayResult(List<Place> list) {

        String curestID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        currentUserPlaces = new ArrayList<>();
        otherUserPlaces = new ArrayList<>();

        for (Place place : list) {
            if (place.getUser().getUserID().equals(curestID)) {

                currentUserPlaces.add(place);
            }
            else {
                otherUserPlaces.add(place);
            }
        }

        progressBar.setVisibility(View.GONE);
        resultList.clear();
        resultList.addAll(currentUserPlaces);

        browPlacesListView.setAdapter(placeAdapter);
        placeAdapter.notifyDataSetChanged();

    }

    @Override
    public void displayError(DatabaseError databaseError) {

    }

    @Override
    public void onMenuActionClick(String menuOption) {
        if (getContext().getString(R.string.browsShowOnMap).equals(menuOption)) {

            detalisFragCallBack.focuseOnMap(browsePlacePreseter);
        }
        if (getContext().getString(R.string.browsEditName).equals(menuOption)) {

            NewPlaceFragment newPlaceFragment = new NewPlaceFragment();

            Bundle bundle = new Bundle();
            bundle.putSerializable(getContext().getString(R.string.editRoute), browsePlacePreseter.getPlace());
            newPlaceFragment.setArguments(bundle);
            detalisFragCallBack.showFragment(newPlaceFragment);

        }
        if (getContext().getString(R.string.detalis).equals(menuOption)) {
            browsePlacePreseter.bulidDetalisBudlier(getContext());
        }

        if (getContext().getString(R.string.broweDelete).equals(menuOption)) {
            Place place = browsePlacePreseter.getPlace();
            String curestID = FirebaseAuth.getInstance().getCurrentUser().getUid();
            if (place.getUser().getUserID().equals(curestID)) {
                placeRepository.remove(place);
                currentUserPlaces.remove(place);
                setListViewConntent(currentUserPlaces);
            }
            else {
                Toast.makeText(getContext(), " Nie możesz usnać nie swojego wpisu!", Toast.LENGTH_SHORT).show();
            }

        }

    }
}
