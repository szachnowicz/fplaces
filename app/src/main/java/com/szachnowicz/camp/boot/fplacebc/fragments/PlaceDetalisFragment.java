package com.szachnowicz.camp.boot.fplacebc.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.google.android.gms.maps.model.LatLng;
import com.szachnowicz.camp.boot.fplacebc.R;
import com.szachnowicz.camp.boot.fplacebc.fragments.detalisRootFragment.BrowsePlacesFragment;
import com.szachnowicz.camp.boot.fplacebc.fragments.detalisRootFragment.NewPlaceFragment;
import com.szachnowicz.camp.boot.fplacebc.fragments.detalisRootFragment.OptionsFragment;
import com.szachnowicz.camp.boot.fplacebc.fragments.detalisRootFragment.PlaceAroundFragment;
import com.szachnowicz.camp.boot.fplacebc.presenter.NewPlacePreseter;
import com.szachnowicz.camp.boot.fplacebc.presenter.impl.DetalisPresenter;
import com.szachnowicz.camp.boot.fplacebc.view.fragmentsCallback.DetalisFragCallBack;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlaceDetalisFragment extends Fragment {

    @BindView(R.id.browsePlace)
    Button browsePlaceBtn;

    @BindView(R.id.placeAround)
    Button placeAroundBtn;

    @BindView(R.id.addNewPlaceBtn)
    Button addNewPlaceBtn;

    @BindView(R.id.opptionsBtn)
    ImageButton opptionsBtn;

    @BindView(R.id.browseFrameLayout)
    FrameLayout frameLayoutforFragments;

    private BrowsePlacesFragment browsePlacesFragment;
    private NewPlaceFragment newPlaceFragment;
    private PlaceAroundFragment placeAroundFragment;
    private OptionsFragment optionsFragment;

    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;
    private DetalisFragCallBack detalisFragCallBack;
    private boolean showEditMapButtons = false;
    private View view;
    private Snackbar snackbar;

    public PlaceDetalisFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_place_detalis, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        snackbar = Snackbar.make(view, "", Snackbar.LENGTH_LONG);

        placeAroundFragment = new PlaceAroundFragment();
        newPlaceFragment = new NewPlaceFragment();
        browsePlacesFragment = new BrowsePlacesFragment();
        optionsFragment = new OptionsFragment();

    }

    public void hideUnnesseryUI(boolean show) {

        if (show) {
            addNewPlaceBtn.setVisibility(View.VISIBLE);
            opptionsBtn.setVisibility(View.VISIBLE);
            browsePlaceBtn.setVisibility(View.VISIBLE);
            placeAroundBtn.setVisibility(View.VISIBLE);

        }
        else {
            addNewPlaceBtn.setVisibility(View.GONE);
            opptionsBtn.setVisibility(View.GONE);
            placeAroundBtn.setVisibility(View.GONE);
            browsePlaceBtn.setVisibility(View.GONE);

        }

    }

    @OnClick(R.id.addNewPlaceBtn)
    public void onClickAddNewPlaceBtn(View view) {
        newPlaceFragment = new NewPlaceFragment();
        showEditMapButtons = false;
        detalisFragCallBack.showFragment(newPlaceFragment);
    }

    @OnClick(R.id.opptionsBtn)
    public void onClickoOptionsBtn(View view) {
        detalisFragCallBack.showFragment(optionsFragment);
    }

    @OnClick(R.id.placeAround)
    public void onClickPlaceAround(View view) {
        detalisFragCallBack.showFragment(placeAroundFragment);
    }

    @OnClick(R.id.browsePlace)
    public void onClickBrowsPlace(View view) {
        detalisFragCallBack.showFragment(browsePlacesFragment);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity;
        if (context instanceof Activity) {
            activity = (Activity) context;
            try {

                detalisFragCallBack = (DetalisFragCallBack) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString() + " must implement OnSelected");

            }
        }
    }

    public void controlNewPlaceFragment() {
        showEditMapButtons = !showEditMapButtons;
        newPlaceFragment.showEditMapButtons(showEditMapButtons);
    }

    public void dispatchPresenters(DetalisPresenter detalisPreseter) {
        if (detalisPreseter.ID == NewPlacePreseter.NP_PRES_ID && newPlaceFragment != null) {
            newPlaceFragment.updateGuiWithPreseter((NewPlacePreseter) detalisPreseter);
        }

    }

    public void showSnackBarWithMessage(String message) {
        if (snackbar != null && message != "") {
            snackbar.setText(message);
            snackbar.show();

        }
    }

    public void controlBrowsePlaceFragmet(boolean mapVisible) {
        browsePlacesFragment.hideUnesseryUI(mapVisible);

    }

    public void setNewPlaceFramgent(NewPlaceFragment newPlaceFramgent) {
        this.newPlaceFragment = newPlaceFramgent;
    }

    public void passLastTrackedPossiton(LatLng latLng) {
        if(placeAroundFragment != null)
        {
            placeAroundFragment.setLastTrackedPossiton(latLng);
        }
    }
}
