package com.szachnowicz.camp.boot.fplacebc.fragments.detalisRootFragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.szachnowicz.camp.boot.fplacebc.R;
import com.szachnowicz.camp.boot.fplacebc.entities.Place;
import com.szachnowicz.camp.boot.fplacebc.presenter.NewPlacePreseter;
import com.szachnowicz.camp.boot.fplacebc.services.PlaceRepository;
import com.szachnowicz.camp.boot.fplacebc.view.fragmentsCallback.DetalisFragCallBack;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewPlaceFragment extends Fragment {

    public static final int FRAGMNET_ID = 2;
    @BindView(R.id.locationMapFocuseBtn)
    ImageButton locationMapFocuseButton;
    @BindView(R.id.placeNameEt)
    EditText placeNameExitText;
    @BindView(R.id.placeDiscriptionEt)
    EditText placeDiscrptionEditText;
    @BindView(R.id.placeLocattionEt)
    EditText placeLocattionEditText;
    @BindView(R.id.addPointBtn)
    ImageButton addPointBtn;
    @BindView(R.id.clearMapBtn)
    ImageButton clearMapBtn;
    @BindView(R.id.createRouteBtn)
    ImageButton createRouteBtn;
    @BindView(R.id.choosenOnMapLay)
    LinearLayout choosenOnMapLayout;
    @BindView(R.id.choosenOnMapDetalisBtn)
    Button choosenOnMapDetalisBtn;
    @BindView(R.id.saveNewPlaceBtn)
    Button saveNewPlaceBtn;
    private boolean fragmentReady = false;
    private DetalisFragCallBack detalisFragCallBack;
    private NewPlacePreseter newPlacePreseter;
    private View view;

    public NewPlaceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        showEditMapButtons(false);
        newPlacePreseter = new NewPlacePreseter();
        fragmentReady = true;
        Bundle arguments = getArguments();
        if (arguments != null) {
            Place place = (Place) arguments.getSerializable(getContext().getString(R.string.editRoute));
            setPlaceToEdit(place);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_new_place, container, false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newPlacePreseter.hideKeyboard(view, getContext());
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity;
        if (context instanceof Activity) {
            activity = (Activity) context;
            try {
                detalisFragCallBack = (DetalisFragCallBack) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                                                     + " must implement OnSelected");

            }
        }
    }

    @OnClick(R.id.goBackBtn2)
    public void onClickChangeViewButton(View view) {
        detalisFragCallBack.goBackToMenuFragment("");
    }

    public void updateGuiWithPreseter(NewPlacePreseter newPlacePreseter) {
        this.newPlacePreseter = newPlacePreseter;

        if (!fragmentIsReady())
            return;

        choosenOnMapLayout.setVisibility(View.VISIBLE);
        placeLocattionEditText.setVisibility(View.VISIBLE);
        newPlacePreseter.disbaleEditText(placeLocattionEditText);

        if (newPlacePreseter.routeOrSinglePoint()) {
            placeLocattionEditText.setText(R.string.choosenRouteOnMap);
            choosenOnMapDetalisBtn.setText(R.string.routeDetalis);
            saveNewPlaceBtn.setVisibility(View.VISIBLE);

        }
        else {
            if (newPlacePreseter.getPlace().getSinglePoint() != null)
                placeLocattionEditText.setText(R.string.choosenPoinOnMap);
            choosenOnMapDetalisBtn.setText(R.string.pointDetalis);
            saveNewPlaceBtn.setVisibility(View.VISIBLE);
        }

    }

    private boolean fragmentIsReady() {
        return fragmentReady;
    }

    @OnClick(R.id.locationMapFocuseBtn)
    public void onClickFocuseMapLocation(View view) {

        newPlacePreseter.colectDataFormView(placeNameExitText, placeDiscrptionEditText);
        detalisFragCallBack.focuseOnMap(newPlacePreseter);
        newPlacePreseter.hideKeyboard(view, getContext());
        showEditMapButtons(true);

    }

    @OnClick(R.id.locationBrowseBtn)
    public void onClickLocationBrowse(View view) {
        newPlacePreseter.bulidBuliderForSearchAddresApi(getContext());

    }

    public void showEditMapButtons(boolean show) {
        if (show) {
            addPointBtn.setVisibility(View.VISIBLE);
            createRouteBtn.setVisibility(View.VISIBLE);
            clearMapBtn.setVisibility(View.VISIBLE);
            choosenOnMapLayout.setVisibility(View.GONE);
            saveNewPlaceBtn.setVisibility(View.GONE);
        }
        else {
            addPointBtn.setVisibility(View.GONE);
            createRouteBtn.setVisibility(View.GONE);
            clearMapBtn.setVisibility(View.GONE);
        }

    }

    @OnClick(R.id.clearMapBtn)
    public void onClickClearMapBtn(View view) {
        detalisFragCallBack.mapHelpersButtonAction(R.id.clearMapBtn);
    }

    @OnClick(R.id.createRouteBtn)
    public void onClickCreatRouteBtn(View view) {
        detalisFragCallBack.mapHelpersButtonAction(R.id.createRouteBtn);
    }

    @OnClick(R.id.addPointBtn)
    public void onClickAddPointBtn(View view) {
        detalisFragCallBack.mapHelpersButtonAction(R.id.addPointBtn);

    }

    @OnClick(R.id.choosenOnMapDetalisBtn)
    public void onClickChoosenOnMapDetalisBtn(View view) {
        newPlacePreseter.bulidListViewInToast(getContext());
    }

    @OnClick(R.id.saveNewPlaceBtn)
    public void onClickSaveNewPlaceBtn(View view) {
        PlaceRepository placeRepository = new PlaceRepository();
        newPlacePreseter.colectDataFormView(placeNameExitText, placeDiscrptionEditText);
        Place place = newPlacePreseter.getPlace();
        if ( place.getPlaceID()==null) {
            placeRepository.add(place);
        }
        else {
            placeRepository.update(place);
        }
        detalisFragCallBack.goBackToMenuFragment(getContext().getResources().getString(R.string.plaeSaved));

    }

    public void setPlaceToEdit(Place place) {
        newPlacePreseter.setPlace(place);
        placeNameExitText.setText(place.getPlaceName());
        placeDiscrptionEditText.setText(place.getPlaceDisc());
        detalisFragCallBack.focuseOnMap(newPlacePreseter);

    }
}
