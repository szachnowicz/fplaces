package com.szachnowicz.camp.boot.fplacebc.fragments.detalisRootFragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseError;
import com.szachnowicz.camp.boot.fplacebc.R;
import com.szachnowicz.camp.boot.fplacebc.entities.Place;
import com.szachnowicz.camp.boot.fplacebc.presenter.NewPlacePreseter;
import com.szachnowicz.camp.boot.fplacebc.services.MapCalculator;
import com.szachnowicz.camp.boot.fplacebc.services.PlaceRepository;
import com.szachnowicz.camp.boot.fplacebc.services.interfaces.DatabaseCallback;
import com.szachnowicz.camp.boot.fplacebc.view.fragmentsCallback.DetalisFragCallBack;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlaceAroundFragment extends Fragment implements DatabaseCallback {

    public static final int FRAGMNET_ID = 1;

    @BindView(R.id.distanceToPlaceEidTxt)
    EditText distanceToPlaceEidTxt;

    @BindView(R.id.howManyPlaceAroundTxt)
    EditText howManyPlaceAroundTxt;

    @BindView(R.id.spinnerWithDistance)
    Spinner spinner;

    @BindView(R.id.textInputLayout1)
    TextInputLayout textInputLayout1;
    @BindView(R.id.progressBarPlaceAround)
    ProgressBar progressBar;

    private DetalisFragCallBack detalisFragCallBack;
    private PlaceRepository placeRepository;
    private LatLng lastTrackedPossiton;
    private List<Place> restulList;
    private ArrayAdapter<CharSequence> adapter;
    private String currentDistance;

    public PlaceAroundFragment() {

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        placeRepository = new PlaceRepository();
        placeRepository.getAllResults(this);
        NewPlacePreseter.disbaleEditText(distanceToPlaceEidTxt);
        NewPlacePreseter.disbaleEditText(howManyPlaceAroundTxt);

        initSpinner();
    }

    private void initSpinner() {
        adapter =
                ArrayAdapter.createFromResource(getContext(),
                                                R.array.placeDistance, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (restulList != null) {
                    String hint = getContext().getString(R.string.placeAroundUser) + " " + adapter.getItem(position) + " " + getContext().getString(R.string.placeAroundKM);
                    textInputLayout1.setHint(hint);
                    changeDistanceAndDisplayPoints(adapter.getItem(position).toString());
                    currentDistance = adapter.getItem(position).toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        currentDistance = adapter.getItem(0).toString();

    }

    private void changeDistanceAndDisplayPoints(String item) {
        double distanceInKm = Integer.parseInt(item.substring(0, item.length() - 2));

        List<Place> places = displayPointForGivenDistane(item);
        if (getContext() != null) {
            howManyPlaceAroundTxt.setText(places.size() + " " + getContext().getString(R.string.placeInNeiberhood));
        }
        detalisFragCallBack.drawCircleAddMarkersToMap(places, distanceInKm);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_place_around, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity;
        if (context instanceof Activity) {
            activity = (Activity) context;
            try {

                detalisFragCallBack = (DetalisFragCallBack) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                                                     + " must implement OnSelected");

            }
        }
    }

    @OnClick(R.id.goBackBtn3)
    public void onClickChangeViewButton(View view) {
        detalisFragCallBack.goBackToMenuFragment("");
    }

    @Override
    public void displayResult(List<Place> list) {
        progressBar.setVisibility(View.GONE);
        restulList = list;
        changeDistanceAndDisplayPoints(adapter.getItem(0).toString());
        String hint = getContext().getString(R.string.placeAroundUser) + " " + adapter.getItem(0).toString()+ " " + getContext().getString(R.string.placeAroundKM);
        textInputLayout1.setHint(hint);
    }

    @Override
    public void displayError(DatabaseError databaseError) {

    }

    public void setLastTrackedPossiton(LatLng lastTrackedPossiton) {
        this.lastTrackedPossiton = lastTrackedPossiton;
    }

    public List<Place> displayPointForGivenDistane(String distane) {

        int distanceInKm = Integer.parseInt(distane.substring(0, distane.length() - 2));
        List<Place> list = new ArrayList<Place>();
        for (Place place : restulList) {
            double distance = getDistance(place);
            if (distance < distanceInKm) {
                list.add(place);
            }

        }

        return list;
    }

    private double getDistance(Place place) {
        LatLng placeLatLng;
        if (place.isSinglePointOnly()) {
            placeLatLng = place.getSinglePoint().getLatLng();
        }
        else {
            placeLatLng = place.getRouteList().get(0).getLatLng();
        }

        return MapCalculator.getKmPointToLnglat(lastTrackedPossiton, placeLatLng);
    }

    @OnClick(R.id.showOnlyRoute)
    public void onClickShowOnlyRoute(View view) {
        if (restulList == null) {
            return;
        }
        detalisFragCallBack.mapHelpersButtonAction(R.id.showOnlyRoute);
        changeDistanceAndDisplayPoints(currentDistance);

    }

    @OnClick(R.id.showAllBtn)
    public void onClickShowAllBtn(View view) {
        if (restulList == null) {
            return;
        }
        detalisFragCallBack.focuseOnMap(null);
        detalisFragCallBack.mapHelpersButtonAction(R.id.showAllBtn);
        changeDistanceAndDisplayPoints(currentDistance);
    }

    @OnClick(R.id.showOnlyPoint)
    public void onClickShowOnlyPoint(View view) {
        if (restulList == null) {
            return;
        }
        detalisFragCallBack.mapHelpersButtonAction(R.id.showOnlyPoint);
        changeDistanceAndDisplayPoints(currentDistance);
    }

}
