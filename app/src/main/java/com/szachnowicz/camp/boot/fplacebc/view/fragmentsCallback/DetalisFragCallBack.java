package com.szachnowicz.camp.boot.fplacebc.view.fragmentsCallback;

import android.support.v4.app.Fragment;

import com.szachnowicz.camp.boot.fplacebc.entities.Place;
import com.szachnowicz.camp.boot.fplacebc.presenter.impl.DetalisPresenter;

import java.util.List;

/**
 * Created by Sebastian on 2017-08-17.
 */

public interface DetalisFragCallBack {

    void goBackToMenuFragment(String message);
    void showFragment(Fragment fragment);
    void focuseOnMap(DetalisPresenter detalisPresenter);
    void mapHelpersButtonAction(int buttonId);
    void drawCircleAddMarkersToMap(List<Place> places, double distanceInKm);
}
