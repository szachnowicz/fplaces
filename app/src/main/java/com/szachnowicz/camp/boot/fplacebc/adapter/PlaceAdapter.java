package com.szachnowicz.camp.boot.fplacebc.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.szachnowicz.camp.boot.fplacebc.R;
import com.szachnowicz.camp.boot.fplacebc.entities.Place;
import com.szachnowicz.camp.boot.fplacebc.presenter.NewPlacePreseter;

import java.util.List;

/**
 * Created by Sebastian on 2017-08-23.
 */

public class PlaceAdapter extends ArrayAdapter<Place> {

    private final Context context;
    private final List<Place> resourseList;
    private EditText adapterPlaceType;
    private EditText adapterPlaceName;
    private EditText adapterPlaceDiscription;

    public PlaceAdapter(Context context, List<Place> placeList) {
        super(context, -1, placeList);
        this.context = context;
        resourseList = placeList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.place_list_adatper, parent, false);
        adapterPlaceType = (EditText) rowView.findViewById(R.id.adapterPlaceType);
        adapterPlaceName = (EditText) rowView.findViewById(R.id.adapterPlaceName);
        adapterPlaceDiscription = (EditText) rowView.findViewById(R.id.adapterPlaceDiscription);
        NewPlacePreseter.disbaleEditText(adapterPlaceType);
        NewPlacePreseter.disbaleEditText(adapterPlaceName);
        NewPlacePreseter.disbaleEditText(adapterPlaceDiscription);

        Place place = resourseList.get(position);

        if (place.isSinglePointOnly()) {
            adapterPlaceType.setText(context.getString(R.string.placeTypePlace));
            adapterPlaceType.setTextColor(getContext().getResources().getColor(R.color.blue));
        }
        else {
            adapterPlaceType.setText(context.getString(R.string.placeTypeRoute));
            adapterPlaceType.setTextColor(getContext().getResources().getColor(R.color.green));
            adapterPlaceName.setHint(context.getString(R.string.routeName));
        }

        adapterPlaceName.setText(place.getPlaceName());
        adapterPlaceDiscription.setText(place.getPlaceDisc());
        return rowView;
    }
}
