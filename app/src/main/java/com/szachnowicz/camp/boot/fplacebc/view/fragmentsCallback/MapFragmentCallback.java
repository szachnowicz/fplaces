package com.szachnowicz.camp.boot.fplacebc.view.fragmentsCallback;

import com.szachnowicz.camp.boot.fplacebc.presenter.impl.DetalisPresenter;

/**
 * Created by Sebastian on 2017-08-15.
 */

public interface MapFragmentCallback {
    void changeFragmentSize();
    void retrunPreseterToMainActivity(DetalisPresenter detalisPreseter);
}
