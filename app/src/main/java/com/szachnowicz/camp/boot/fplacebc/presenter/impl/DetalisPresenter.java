package com.szachnowicz.camp.boot.fplacebc.presenter.impl;

import com.szachnowicz.camp.boot.fplacebc.entities.Place;
import com.szachnowicz.camp.boot.fplacebc.entities.User;

/**
 * Created by Sebastian on 2017-08-18.
 */

public abstract class DetalisPresenter {

    public int ID;
    protected Place place;
    protected User user;

    public DetalisPresenter(int ID) {
        this.ID = ID;
        // // TODO: 2017-08-19  chage when user will be used 
        place = new Place();
    }

    public int getID() {
        return ID;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public User getUser() {
        return user;
    }
}
