package com.szachnowicz.camp.boot.fplacebc.presenter;

import android.graphics.Color;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;
import com.szachnowicz.camp.boot.fplacebc.R;
import com.szachnowicz.camp.boot.fplacebc.entities.Place;
import com.szachnowicz.camp.boot.fplacebc.entities.SinglePoint;
import com.szachnowicz.camp.boot.fplacebc.services.MapCalculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Sebastian on 2017-08-25.
 */

public class MapPresenter {

    private List<Marker> markerList = new ArrayList<>();
    private boolean showOnlyRoute = true;
    private boolean showOnlyPoint = true;

    public void drawCircleOnMap(GoogleMap map, LatLng currentLatLng, double widthCircle) {

        map.clear();
        Circle circle = map.addCircle(new CircleOptions()
                                              .center(currentLatLng)
                                              .fillColor(Color.argb(10, 255, 15, 40))
                                              .strokeColor(Color.argb(50, 255, 15, 40))
                                              .strokeWidth(5)
                                              .zIndex(1));
        ;
        circle.setRadius(widthCircle * 1000);

    }

    public void changeCameraAndBerring(LatLng from, LatLng to, GoogleMap googleMap) {
        float targetBearing = MapCalculator.bearingBetweenLatLngs(from, SphericalUtil.interpolate(from, to, 0.5));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(SphericalUtil.interpolate(from, to, 0.5))
                .zoom(18)
                .bearing(targetBearing)
                .tilt(90)
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public void drawRouteOnMap(Place place, GoogleMap googleMap) {

        if (!place.getRouteList().isEmpty()) {

            addStartMarkerToRoute(place, googleMap);
            PolylineOptions options = new PolylineOptions();

            for (SinglePoint point : place.getRouteList()) {
                MarkerOptions marek = new MarkerOptions();
                marek.position(point.getLatLng());
                marek.snippet(place.getPlaceDisc());
                marek.title(place.getPlaceName());

                if (!options.getPoints().contains(point))
                    options.add(point.getLatLng());
            }
            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            options.width(12).color(color);
            googleMap.addPolyline(options);
        }
    }

    private Marker addStartMarkerToRoute(Place singlePoint, GoogleMap googleMap) {

        MarkerOptions startMarkerOptions = new MarkerOptions();
        startMarkerOptions.position(singlePoint.getRouteList().get(0).getLatLng());
        startMarkerOptions.title(singlePoint.getPlaceName());
        startMarkerOptions.snippet(singlePoint.getPlaceDisc());
        startMarkerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.start_ico));
        return googleMap.addMarker(startMarkerOptions);
    }

    public void drawAllPlaces(List<Place> places, GoogleMap googleMap) {
        for (Place place : places) {
            MarkerOptions marekOpt = new MarkerOptions();
            Marker marker;
            marekOpt.title(place.getPlaceName());
            marekOpt.snippet(place.getPlaceDisc());

            if (place.isSinglePointOnly()) {
                if (showOnlyPoint) {
                    marekOpt.position(place.getSinglePoint().getLatLng());
                    marekOpt.icon(BitmapDescriptorFactory.fromResource(R.drawable.big_red_pin));
                }
            }
            else {
                if (showOnlyRoute) {
                    drawRouteOnMap(place, googleMap);
                }
                marekOpt.position(place.getRouteList().get(0).getLatLng());
                marekOpt.icon(BitmapDescriptorFactory.fromResource(R.drawable.start_ico));
            }
            if (marekOpt.getPosition() != null) {
                marker = googleMap.addMarker(marekOpt);
                markerList.add(marker);
            }
        }

    }

    public boolean isShowOnlyRoute() {
        return showOnlyRoute;
    }

    public void setShowOnlyRoute(boolean showOnlyRoute) {
        this.showOnlyRoute = showOnlyRoute;
    }

    public boolean isShowOnlyPoint() {
        return showOnlyPoint;
    }

    public void setShowOnlyPoint(boolean showOnlyPoint) {
        this.showOnlyPoint = showOnlyPoint;
    }
}
