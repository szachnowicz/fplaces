package com.szachnowicz.camp.boot.fplacebc.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;
import com.szachnowicz.camp.boot.fplacebc.R;
import com.szachnowicz.camp.boot.fplacebc.entities.Place;
import com.szachnowicz.camp.boot.fplacebc.entities.SinglePoint;
import com.szachnowicz.camp.boot.fplacebc.presenter.MapPresenter;
import com.szachnowicz.camp.boot.fplacebc.presenter.NewPlacePreseter;
import com.szachnowicz.camp.boot.fplacebc.presenter.impl.DetalisPresenter;
import com.szachnowicz.camp.boot.fplacebc.view.fragmentsCallback.MapFragmentCallback;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapFragment extends Fragment implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleMap.OnMapClickListener {

    @BindView(R.id.changeViewButton)
    ImageButton switchButton;
    @BindView(R.id.MapView)
    MapView mMapView;
    private MapFragmentCallback mapCallback;
    private GoogleMap googleMap;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private View view;

    private DetalisPresenter detalisPreseter;
    private MarkerOptions currentFocuseMarker;
    private Marker currentMarker;
    private boolean allowCreatingRoutes = false;
    private boolean allowAddingPointToMap = false;
    private LatLng lastTracedLcation;
    private Place place;
    private MapPresenter mapPresenter;
    private boolean trackLocation = true;

    public MapFragment() {

    }

    public LatLng getLastTracedLcation() {
        return lastTracedLcation;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }
        place = new Place();
        mapPresenter = new MapPresenter();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_map, container, false);
        return view;
    }

    public void changeButtonImge(boolean left) {
        if (left) {
            switchButton.setImageResource(R.mipmap.left);

        }
        else {
            switchButton.setImageResource(R.mipmap.right);
        }

    }

    @OnClick(R.id.changeViewButton)
    public void onClickChangeViewButton(View view) {
        if (detalisPreseter != null) {
            detalisPreseter.setPlace(place);
            mapCallback.retrunPreseterToMainActivity(detalisPreseter);
        }
        mapCallback.changeFragmentSize();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity;
        if (context instanceof Activity) {
            activity = (Activity) context;
            try {

                mapCallback = (MapFragmentCallback) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                                                     + " must implement OnSelected");

            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(
                getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

        LatLng latlng = new LatLng(location.getLatitude(), location.getLongitude());
        if (trackLocation) {
            if (googleMap != null) {
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(16));
                lastTracedLcation = latlng;
            }
        }
        lastTracedLcation = latlng;
    }

    @Override
    public void onMapClick(LatLng latLng) {

        if (allowAddingPointToMap) {
            addSingleMarkerToRoute(latLng);
        }
        if (allowCreatingRoutes) {
            createRoute(latLng);
        }

    }

    private void addSingleMarkerToRoute(LatLng latLng) {
        if (currentMarker == null) {
            currentFocuseMarker = new MarkerOptions();
            currentFocuseMarker.draggable(false);
            currentFocuseMarker.position(latLng);
            currentFocuseMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.big_red_pin));
            currentMarker = googleMap.addMarker(currentFocuseMarker);
        }
        googleMap.clear();
        currentMarker = googleMap.addMarker(currentFocuseMarker);
        currentMarker.setPosition(latLng);
        SinglePoint siglePoint = new SinglePoint();
        siglePoint.setPossiton(latLng);

        place.setSinglePoint(siglePoint);
    }

    private void createRoute(LatLng latLng) {
        SinglePoint siglePoint = new SinglePoint();
        siglePoint.setPossiton(latLng);
        if (place.getRouteList().size() > 1) {
            SinglePoint irterpolatePoint = new SinglePoint();
            LatLng from = place.getRouteList().get(place.getRouteList().size() - 1).getLatLng();
            irterpolatePoint.setPossiton(SphericalUtil.interpolate(from, latLng, 0.5));
            place.getRouteList().add(irterpolatePoint);
        }
        place.getRouteList().add(siglePoint);
        googleMap.clear();
        mapPresenter.drawRouteOnMap(place, googleMap);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getContext());
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        this.googleMap.setOnMapClickListener(this);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                this.googleMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            this.googleMap.setMyLocationEnabled(true);
        }

    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public void zoomMapToPoint(Place place) {
        this.place = place;
        googleMap.clear();
        mapPresenter.changeCameraAndBerring(lastTracedLcation, place.getSinglePoint().getLatLng(), googleMap);
        LatLng latLng = place.getSinglePoint().getLatLng();
        if (googleMap != null) {
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(16));
        }

        addSingleMarkerToRoute(latLng);

    }

    public void setDetalisPrsenter(DetalisPresenter detalisPreseter) {
        this.detalisPreseter = detalisPreseter;
        trackLocation = false;
    }

    public void newPlaceButtonCallback(int buttonId) {
        trackLocation = false;

        if (buttonId == R.id.addPointBtn) {
            allowAddingPointToMap = true;
            allowCreatingRoutes = false;
            place = new Place();
            googleMap.clear();
        }

        if (buttonId == R.id.createRouteBtn) {
            allowCreatingRoutes = true;
            allowAddingPointToMap = false;
            currentMarker = null;
            place = new Place();
            googleMap.clear();
        }

        if (buttonId == R.id.clearMapBtn) {
            clearLastPoint();
        }

        if (buttonId == R.id.showOnlyRoute) {
            mapPresenter.setShowOnlyRoute(true);
            mapPresenter.setShowOnlyPoint(false);
            googleMap.clear();
        }
        if (buttonId == R.id.showAllBtn) {
            mapPresenter.setShowOnlyRoute(true);
            mapPresenter.setShowOnlyPoint(true);
        }
        if (buttonId == R.id.showOnlyPoint) {
            mapPresenter.setShowOnlyRoute(false);
            mapPresenter.setShowOnlyPoint(true);
            googleMap.clear();
        }

    }

    private void clearLastPoint() {
        if (allowAddingPointToMap) {
            googleMap.clear();
            place = new Place();
        }

        if (allowCreatingRoutes) {
            if (place.getRouteList().size() > 1) {
                int index = place.getRouteList().size() - 1;
                place.getRouteList().remove(index);
                mapPresenter.drawRouteOnMap(place, googleMap);
            }

        }
    }

    public void resetToDefaul() {
        googleMap.clear();
        allowAddingPointToMap = false;
        currentFocuseMarker = null;
        currentMarker = null;
        allowCreatingRoutes = false;
        place = new Place();
        trackLocation = true;

    }

    public void annimateTheRoute(final Place place) {
        mapPresenter.drawRouteOnMap(place, googleMap);

        this.place = place;

        LatLng latLng = place.getRouteList().get(0).getLatLng();
        if (googleMap != null) {
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(16));
        }

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {

                super.handleMessage(msg);
                if (trackLocation)
                    return;
                int beginIdx = (int) msg.obj;
                LatLng from = place.getRouteList().get(beginIdx - 1).getLatLng();
                LatLng to = place.getRouteList().get(beginIdx).getLatLng();
                mapPresenter.changeCameraAndBerring(from, to, googleMap);
            }
        };
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int icrementBy = 1;
                    int currentPoint = 1;
                    Thread.sleep(1000);
                    while (currentPoint < place.getRouteList().size()) {
                        Message message = new Message();
                        Thread.sleep(200);
                        message.obj = currentPoint;

                        currentPoint += icrementBy;
                        handler.sendMessage(message);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }).start();

    }

    public void drawCircleAddMarkers(List<Place> places, double distanceInKm) {

        Place firstPlace = places.get(0);
        LatLng firstLatLng = firstPlace.getSinglePoint() != null ? firstPlace.getSinglePoint().getLatLng() : firstPlace.getRouteList().get(0).getLatLng();
        trackLocation = false;
        mapPresenter.drawCircleOnMap(googleMap, lastTracedLcation, distanceInKm);
        mapPresenter.drawAllPlaces(places, googleMap);
        setUpMarkerInfoWindow(places);
        mapPresenter.changeCameraAndBerring(lastTracedLcation, firstLatLng, googleMap);

    }

    private void setUpMarkerInfoWindow(final List<Place> places) {

        if (googleMap != null) {

            googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    if (marker.getTitle() == null)
                        return null;

                    View view = getLayoutInflater(getArguments()).inflate(R.layout.marek_window, null);
                    view.setLayoutParams(new LinearLayout.LayoutParams(800, 400));
                    EditText placeName = (EditText) view.findViewById(R.id.placeNameEtMarkerWindo);
                    EditText placeDiscr = (EditText) view.findViewById(R.id.placeDiscriptionEtMarkerWindo);
                    NewPlacePreseter.disbaleEditText(placeDiscr);
                    NewPlacePreseter.disbaleEditText(placeName);
                    LatLng latLng = marker.getPosition();
                    placeName.setText(marker.getTitle());
                    placeDiscr.setText(marker.getSnippet());
                    String pathToFile = null;

                    return view;
                }
            });

        }

    }

}

