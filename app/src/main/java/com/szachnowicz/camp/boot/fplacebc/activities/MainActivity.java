package com.szachnowicz.camp.boot.fplacebc.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.szachnowicz.camp.boot.fplacebc.R;
import com.szachnowicz.camp.boot.fplacebc.entities.Place;
import com.szachnowicz.camp.boot.fplacebc.fragments.MapFragment;
import com.szachnowicz.camp.boot.fplacebc.fragments.PlaceDetalisFragment;
import com.szachnowicz.camp.boot.fplacebc.fragments.detalisRootFragment.BrowsePlacesFragment;
import com.szachnowicz.camp.boot.fplacebc.fragments.detalisRootFragment.NewPlaceFragment;
import com.szachnowicz.camp.boot.fplacebc.fragments.detalisRootFragment.PlaceAroundFragment;
import com.szachnowicz.camp.boot.fplacebc.presenter.impl.DetalisPresenter;
import com.szachnowicz.camp.boot.fplacebc.view.fragmentsCallback.DetalisFragCallBack;
import com.szachnowicz.camp.boot.fplacebc.view.fragmentsCallback.MapFragmentCallback;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MapFragmentCallback, DetalisFragCallBack {

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    // ButterKnife Stuff
    @BindView(R.id.mapFrameLayout)
    FrameLayout frameLayoutMap;
    // ButterKnife Stuff
    @BindView(R.id.detalisFragmentLayout)
    FrameLayout frameLayoutDetalis;
    private ViewGroup.LayoutParams paramsMapLayout;
    private ViewGroup.LayoutParams paramsDetalisLayout;
    private MapFragment mapFragment;
    private PlaceDetalisFragment placeDetalisFragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;
    private DisplayMetrics metrics;
    private DetalisPresenter detalisPreseter;
    private Fragment lastFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        configreTheFragmentsVisibilty();
        requestPermission();

    }

    private void configreTheFragmentsVisibilty() {
        fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();
        mapFragment = new MapFragment();
        placeDetalisFragment = new PlaceDetalisFragment();
        transaction.add(R.id.mapFrameLayout, mapFragment);
        transaction.add(R.id.detalisFragmentLayout, placeDetalisFragment);
        transaction.commit();
        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        configreTheFragmentsSize(metrics.widthPixels / 7, metrics.widthPixels - metrics.widthPixels / 7);
    }

    private void configreTheFragmentsSize(int mapFragmetnWidht, int dtFragmetnWidht) {
        paramsMapLayout = frameLayoutMap.getLayoutParams();
        paramsDetalisLayout = frameLayoutDetalis.getLayoutParams();

        paramsMapLayout.width = mapFragmetnWidht;
        paramsMapLayout.height = metrics.heightPixels;

        paramsDetalisLayout.width = dtFragmetnWidht;
        paramsDetalisLayout.height = metrics.heightPixels;

        frameLayoutDetalis.setLayoutParams(paramsDetalisLayout);
        frameLayoutMap.setLayoutParams(paramsMapLayout);

    }

    @Override
    public void changeFragmentSize() {

        controllRootFragments();

        if (frameLayoutMap.getWidth() > metrics.widthPixels / 2) {
            // hide the Map Fragment
            hideTheMapFragment();
        }
        else
            if (frameLayoutDetalis.getWidth() > metrics.widthPixels / 2) {
                // hide the DetalisFramgnet
                placeDetalisFragment.hideUnnesseryUI(false);
                configreTheFragmentsSize(metrics.widthPixels - metrics.widthPixels / 7, metrics.widthPixels / 7);
                mapFragment.changeButtonImge(true);
            }

    }

    private void controllRootFragments() {

        boolean mapVisbilty = frameLayoutMap.getWidth() < metrics.widthPixels / 2;

        if (lastFragment instanceof NewPlaceFragment) {
            // show only buttons with a menu
            placeDetalisFragment.controlNewPlaceFragment();
        }

        if (lastFragment instanceof BrowsePlacesFragment) {
            placeDetalisFragment.controlBrowsePlaceFragmet(mapVisbilty);
        }

    }

    private void hideTheMapFragment() {
        configreTheFragmentsSize(metrics.widthPixels / 7, metrics.widthPixels - metrics.widthPixels / 7);
        frameLayoutDetalis.setVisibility(View.VISIBLE);
        placeDetalisFragment.hideUnnesseryUI(true);
        mapFragment.changeButtonImge(false);
    }

    @Override
    public void retrunPreseterToMainActivity(DetalisPresenter detalisPreseter) {
        // Passing the preseter back to DetalisFragment
        placeDetalisFragment.dispatchPresenters(detalisPreseter);
    }

    @Override
    public void goBackToMenuFragment(String message) {
        mapFragment.resetToDefaul();
        showFragment(placeDetalisFragment);
        hideTheMapFragment();
        placeDetalisFragment.showSnackBarWithMessage(message);

    }

    @Override
    public void showFragment(Fragment fragment) {
        lastFragment = fragment;
        if (fragment instanceof NewPlaceFragment) {
            placeDetalisFragment.setNewPlaceFramgent((NewPlaceFragment) fragment);
        }
        if (fragment instanceof PlaceAroundFragment) {
            LatLng latLng = mapFragment.getLastTracedLcation();
            placeDetalisFragment.passLastTrackedPossiton(latLng);
        }

        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.detalisFragmentLayout, fragment);
        transaction.commit();

    }

    @Override
    public void focuseOnMap(DetalisPresenter dPreseter) {
        detalisPreseter = dPreseter;
        mapFragment.setDetalisPrsenter(detalisPreseter);
        changeFragmentSize();

        Place place = dPreseter.getPlace();
        if (place.getSinglePoint() != null) {
            mapFragment.zoomMapToPoint(place);

        }
        else
            if (!place.getRouteList().isEmpty()) {
                mapFragment.annimateTheRoute(place);
            }

    }

    @Override
    public void mapHelpersButtonAction(int buttonId) {
        mapFragment.newPlaceButtonCallback(buttonId);

    }

    @Override
    public void drawCircleAddMarkersToMap(List<Place> places, double distanceInKm) {
        changeFragmentSize();
        mapFragment.drawCircleAddMarkers(places, distanceInKm);


    }

    @Override
    public void onBackPressed() {

        if (lastFragment instanceof PlaceDetalisFragment == false) {
            showFragment(placeDetalisFragment);
        }
        else {
            showClosingAppDialog();
        }

    }

    private void showClosingAppDialog() {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Wyjście ");
        alert.setMessage("Chesz zamknąć Aplikacje ?");
        alert.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                finishAndRemoveTask();
                dialog.dismiss();
            }
        });
        alert.setNegativeButton("Nie", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });
//        alert.setNeutralButton("Dodwanie POI", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                RouteDetalisFragment routeD = (RouteDetalisFragment) mSectionsPagerAdapter.getItem(1);
//                routeD.saveRoute();
//                intent.putExtra(ChooseRouteActivity.ROUTE_ID, ROUTE_ID_REFERENCE_KEY);
//                startActivity(intent);
//
//            }
//        });
        alert.show();
    }

    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat
                    .requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_ASK_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    finishAndRemoveTask();
                    Toast.makeText(MainActivity.this, "Permission Granted", Toast.LENGTH_SHORT)
                            .show();
                }
                else {
                    // Permission Denied
                    Toast.makeText(MainActivity.this, "Permission Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
