package com.szachnowicz.camp.boot.fplacebc.entities;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Sebastian on 2017-08-18.
 */

public class Place implements Serializable {

    public static final String TABEL_NAME = "PLACES";
    private static final long serialVersionUID = -2263051469151123394L;
    private String placeID;
    private User user;
    private String placeName;
    private String placeDisc;
    private Date created;
    private List<SinglePoint> routeList;
    private SinglePoint singlePoint;

    public Place(User user) {
        this.user = user;
        routeList = new ArrayList<>();
    }

    public Place() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            setUserFromFB(user);
        }
        routeList = new ArrayList<>();

    }

    public String getPlaceID() {
        return placeID;
    }

    public void setPlaceID(String placeID) {
        this.placeID = placeID;
    }

    public List<SinglePoint> getRouteList() {
        return routeList;
    }

    public void setRouteList(List<SinglePoint> routeList) {
        this.routeList = routeList;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setUserFromFB(FirebaseUser fibeUser) {
        User user = new User();
        user.setName(fibeUser.getDisplayName());
        user.setEmail(fibeUser.getEmail());
        user.setUserID(fibeUser.getUid());
        this.user = user;

    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getPlaceDisc() {
        return placeDisc;
    }

    public void setPlaceDisc(String placeDisc) {
        this.placeDisc = placeDisc;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void addLatLng(LatLng latLng) {
        if (latLng == null) {
            routeList = new ArrayList<>();
        }
        SinglePoint singlePoint = new SinglePoint();
        singlePoint.setPossiton(latLng);
        routeList.add(singlePoint);
    }

    public SinglePoint getSinglePoint() {
        return singlePoint;
    }

    public void setSinglePoint(SinglePoint singlePoint) {
        this.singlePoint = singlePoint;
    }

    public void createSinglePoint(LatLng latLng) {
        singlePoint = new SinglePoint();
        singlePoint.setPossiton(latLng);

    }

    public void setSingelPointNull() {
        singlePoint = null;
    }

    public void addToRouteList(List<LatLng> tempRouteList) {
        for (LatLng lng : tempRouteList) {
            addLatLng(lng);

        }

    }

    public boolean isSinglePointOnly() {
        return singlePoint != null && (routeList == null || routeList.isEmpty());//|| singlePoint==null && !routeList.isEmpty();

    }

    public boolean notFilled() {
        return singlePoint == null && routeList.isEmpty();
    }
}
