package com.szachnowicz.camp.boot.fplacebc.services;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
import com.szachnowicz.camp.boot.fplacebc.entities.SinglePoint;

import java.util.List;

/**
 * Created by Sebastian on 2017-04-22.
 */

public class MapCalculator {

    public static double getRouteLenghtInKm(List<SinglePoint> route) {
        double result = 0;
        for (int i = 0; i < route.size() - 1; i++) {

            result += SphericalUtil.computeDistanceBetween(route.get(i).getLatLng()
                    , route.get(i + 1).getLatLng());
        }

        result = Math.floor(result) / 1000;
        return result;
    }

    // fukcja liczy odległość od punktu do punktu wynik podnwany jest w
    public static double getKmPointToLnglat(LatLng point, LatLng latLng) {
        double result = 0;
        result = SphericalUtil.computeDistanceBetween(point, latLng);
        result = Math.floor(result) / 1000;
        return result;
    }

//    // fukcja liczy odległość od punktu do punktu wynik podnwany jest w  metrach
//    public static double getDistanceFromPointToPoint(Point pointA, Point pointB) {
//        double result = 0;
//        result = SphericalUtil.computeDistanceBetween(pointA.getLatLng(), pointB.getLatLng());
//        result = Math.floor(result) / 100;
//        return result;
//    }

    public static double calculateBearing(LatLng pointA, LatLng pointB) {
        double longitude1 = pointA.longitude;
        double longitude2 = pointB.longitude;
        double latitude1 = Math.toRadians(pointA.longitude);
        double latitude2 = Math.toRadians(pointB.longitude);
        double longDiff = Math.toRadians(longitude2 - longitude1);
        double y = Math.sin(longDiff) * Math.cos(latitude2);
        double x = Math.cos(latitude1) * Math.sin(latitude2) - Math.sin(latitude1) * Math.cos(latitude2) * Math.cos(longDiff);

        return (Math.toDegrees(Math.atan2(y, x)) + 360) % 360;
    }

    public static float bearingBetweenLatLngs(LatLng beginLatLng, LatLng endLatLng) {
        Location beginLocation = convertLatLngToLocation(beginLatLng);
        Location endLocation = convertLatLngToLocation(endLatLng);
        return beginLocation.bearingTo(endLocation);
    }

    public static Location convertLatLngToLocation(LatLng latLng) {
        Location location = new Location("someLoc");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
        return location;
    }

}
