package com.szachnowicz.camp.boot.fplacebc.services;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.szachnowicz.camp.boot.fplacebc.entities.Place;
import com.szachnowicz.camp.boot.fplacebc.services.interfaces.DatabaseCallback;
import com.szachnowicz.camp.boot.fplacebc.services.interfaces.OnGetDataListener;
import com.szachnowicz.camp.boot.fplacebc.services.interfaces.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sebastian on 2017-08-22.
 */

public class PlaceRepository implements Repository<Place> {

    public void getAllResults(final DatabaseCallback databaseCallback) {
        new Database().readDataOnce(Place.TABEL_NAME, new OnGetDataListener() {
            final List<Place> list = new ArrayList<>();

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(DataSnapshot dataSnapshot) {

                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot snap : children
                        ) {

                    Place value = snap.getValue(Place.class);

                    list.add(value);

                }
                databaseCallback.displayResult(list);

            }

            @Override
            public void onFailed(DatabaseError databaseError) {
                databaseCallback.displayError(databaseError);

            }
        });
    }

    @Override
    public void add(Place item) {
        Database database = new Database();
        DatabaseReference databaseReference = database.getRefereceToObject(Place.TABEL_NAME, "").push();
        String primaryKey = databaseReference.getKey();
        item.setPlaceID(primaryKey);
        databaseReference.setValue(item);

    }

    @Override
    public void add(Iterable<Place> items) {

    }

    @Override
    public void update(Place item) {
        new Database().getRefereceToObject(Place.TABEL_NAME, item.getPlaceID()).setValue(item);

    }

    @Override
    public void remove(Place item) {
        new Database().getRefereceToObject(Place.TABEL_NAME, item.getPlaceID()).removeValue();

    }

}
