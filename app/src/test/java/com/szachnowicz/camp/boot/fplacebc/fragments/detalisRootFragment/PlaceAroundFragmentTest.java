package com.szachnowicz.camp.boot.fplacebc.fragments.detalisRootFragment;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Sebastian on 2017-08-25.
 */
public class PlaceAroundFragmentTest {

    @Test
    public void stringToIntTest() throws Exception {

        String km10 = "200km";
        String km200 = "100km";

        int value = Integer.parseInt(km10.substring(0, km10.length()-2));
        System.out.println(value);
        assertEquals(200,value);


    }
}